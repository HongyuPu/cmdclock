from random import randint


class randomArtGenerator():
    def __init__(self):
        pass

    def randomArray(self, size):
        artArray = []
        for i in range(size ** 2):
            numint = randint(0, 3)
            artArray.append(numint)  # generates a random number 0 - 3
        return artArray

    def artConverter(self, size):
        art = randomArtGenerator()
        artArray = art.randomArray(size)
        for i in range(len(artArray)):
            if artArray[i] == 0:
                artArray[i] = " "
            elif artArray[i] == 1:
                artArray[i] = "o"
            elif artArray[i] == 2:
                artArray[i] = " "
            else:
                artArray[i] = "+"
        return artArray

    def printArt(self, size):
        art = randomArtGenerator()
        artArray = art.artConverter(size)
        print("=" * (size * 3 - 2))
        for i in range(size):
            for j in range(size):
                print(artArray[i * size + j], " ", end = '')
            print()
        print("=" * (size * 3 - 2))



# art = randomArtGenerator()
# size = int(input("Enter Size: "))
# iterations = int(input("Enter interations: "))
# for i in range(iterations):
#     art.printArt(size)
#     print()

