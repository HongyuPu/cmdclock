import time
from colour import Color
from colorama import Fore
from termcolor import colored
import datetime
import ctypes
from sty import fg, RgbFg
import ctypes





red = Color('red')
blue = Color('blue')
gradient = list(red.range_to(blue, 100))

def hex_to_rgb(hex):
    hex = str(hex).lstrip('#')
    hlen = len(hex)
    return tuple(int(hex[i:int(i + hlen / 3)], 16) for i in range(0, hlen, int(hlen / 3)))

ctypes.windll.kernel32.SetConsoleTextAttribute("Hello", 0x0004)
print("Hello")


timeArray = str(datetime.datetime.now()).split(".")
timeArray = timeArray[0].split(" ")
for i in gradient:
    if list(str(i))[0] == "#":
        rgb = hex_to_rgb(str(i))
        fg.set_style('j', RgbFg(rgb[0], rgb[1], rgb[2]))
        print(fg.j + timeArray[1])